<br>
<br>    
📌

# **🦊GitLab Usage | How to Check GitLab Usage Quotas?**

## **⏺️Get The DevSecOps Platform**
<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/free.jpg'  width="300px"></div>  

- 📚 [Get The DevSecOps Platform](https://about.gitlab.com/pricing/)

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **🗃️Account and Limit**
<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/account.jpg'  width="800px"></div>  

- 📚[Account and limit settings](https://docs.gitlab.com/ee/user/gitlab_com/index.html#account-and-limit-settings)

- 📚[Webhooks](https://docs.gitlab.com/ee/user/gitlab_com/index.html#webhooks)

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **🔎Check Current GitLab Usage Quotas** 

- **<h3>🐾Steps:</h3>**  
  
  - **<h3>🔑Login GitLab</h3>**  
   
  - **<h3>👇Click on "Preferences"</h3>**  
   
  - **<h3>👉Click on "Usage Quotas"</h3>**  
   
- 🎥[How to Quickly Delete Job Artifacts in GitLab? ](https://youtu.be/iqjEUFyWlf4)
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌