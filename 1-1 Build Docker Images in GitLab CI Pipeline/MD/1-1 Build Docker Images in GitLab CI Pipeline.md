# Build Docker Images in GitLab CI Pipeline

## Topics  
- Prerequisite
- What is GitLab CI Pipeline?
- How to Create a CI Pipeline File?
- Check Pipeline Running Result

## Prerequisite   
- GitLab Account
  - Add credit or debit card in the GitLab Account
  - [400 CI/CD minutes per month](https://about.gitlab.com/pricing/) 
  - [Managing your CI/CD Minutes Usage](https://about.gitlab.com/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage)
  - [Usage Quotas](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab)  
- A Project in GitLab 
  - `git clone https://gitlab.com/lian.duan.training/reactuiapp.git`
     - `feature-1-1-Build-Docker-Images-in-Gitlab-CI-Pipeline`   


## What is GitLab CI Pipeline?
- A CI pipeline is a series of steps that streamline the software delivery process. 
![](./images/cicd_pipeline_infograph.png)
## How to Create a CI Pipeline File?
![](./images/yaml.png)
- <image src='./images/yaml.png'  width="80%"> 
- CI Pipeline File Target
  - Build a react-UI-microservices Dock Image
- CI Pipeline File
  - Format is YAML
  - Default name is .gitlab-ci.yml
- CI Pipeline File Content
  - docker_build is CI job name
  - runner image: docker:20.10.16  
  - services: docker:20.10.16-dind
  - build script: docker build -t lianduantraining/react-app:v4 .
## Check Pipeline Running Result
![](./images/pipelineUI.png)
 

