# **🦊Gradle Best Practices in GitLab**

- **📦Use a Gradle Wrapper**  
  - `gradlew`
   
- **✂️Separate Build and Test Stages in GitLab CI/CD**  
   - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/stages.jpg'  width="400px"></div>  

- **🪚No Caching**  
   - `clean  --no-build-cache` 
   - `--no-cache`
- **🔒Lock 3<sup>rd</sup> and  4<sup>rd</sup> Dependency's Exact Versions**  
   - `gradlew dependencies --write-locks`   
  
   - **<h3>- build.gradle.lockfile </h3>**

   - `--refresh-dependencies`

- **🏗️Project: https://gitlab.com/LianDuanTrainingGroup/common-lib**
  
   - **<h3>Branch: main</h3>**


   - **<h3>Gradle 7.5.1</h3>**
   
