<!-- How to Create Multi-project Pipelines in GitLab CI/CD? （Trigger/Downstream）-->

## **Topics**
- <h3>Prerequisite</3> 
- <h3>What is Multi-project Pipelines?</3>
- <h3>How to Design Multi-project Pipelines?</3>
- <h3>How to Create Multi-project Pipelines?</3> 
- <h3>How to Control Downstream in Pipeline?</3>
- <h3>How to Pass Variables from Upstream Pipeline to Downstream Pipeline?</3>
- <h3>Hands-on Demo</3>


## **Prerequisite**  
- <h3>GitLab Account </3>    
  
- <h3>A React UI Project in GitLab </3>     
  - `git clone https://gitlab.com/lian.duan.training/reactuiapp.git`     
     - `[feature-1-6-How-to-Use-Artifacts-in-GitLab-Pipeline]` 


- <h3>A Spring Boot Project in GitLab </3>     
  - `git clone https://gitlab.com/lian.duan.training/dockerspringbootdemo.git`   
     - `[feature-1-9-How-to-Trigger-Downstream-in-GitLab-Pipeline]`  


- <h3>A Release Microservices Project in GitLab </3>     
  - `git clone https://gitlab.com/LianDuanTrainingGroup/multiplepipelines.git`   
     - `[feature-parallel-build]`

## **What is Multi-project Pipelines?**   
- <h3>A pipeline in one project can trigger downstream pipelines in another project, called multi-project pipelines. </h3>


## **How to Design a Multi-project Pipelines?**
- <h3>Analysis Projects</h3>
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/pipelinewithnu.jpg'  width="100%"></div> 


## **How to Create Multi-project Pipelines?**     

- <h3>Create Downstream Pipeline</h3>
- <h3>Create Upstream Pipeline</h3>
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/git6.jpg'  width="100%"></div>    


## **How to Control Downstream Pipeline?**     

- <h3>Add Trigger Strategy </h3>
   - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/git5.jpg'  width="100%"></div>

</br>  

- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/pipelinewithnu.jpg'  width="100%" ></div> 

## **How to Pass Variables from Upstream Pipeline to Downstream Pipeline?**
- <h3> Upstream Pipeline - Release Microservices Project</h3>   
    - <h4>Create Docker Image Tag Variables in Project</h4> 
    - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/git1.jpg' width="100%"></div> 
    - <h4>Pass the Docker Image Tag Value to the Downstream</h4> 
    - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); ><image src='./images/git2.jpg' width="100%"></div>    
 
- <h3>Downstream Pipeline - Spring Boot Project</h3> 
    - <h4>Create Docker Image Tag Variables in Project</h4> 
    -  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/git3.jpg' width="100%"></div> 
   - <h4>Use Docker Image Tag Value in the Docker command </h4> 
   -  <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/git4.jpg' width="100%"></div> 




## **Hands-on Demo**
- <h3>Triage Multi-project Pipelines</h3> 
- <h3>Check Build Docker Images in Dockerhub</h3> 

