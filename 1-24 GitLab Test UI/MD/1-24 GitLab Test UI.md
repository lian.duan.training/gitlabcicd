
<br>
<br>    
📌
<!--Unit Test Reports| GitLab JUnit Test Reports | GitLab Tutorial-->

# **🦊GitLab Unit Test Reports**
## **📤Topics:**

- **<h3>🚩GitLab Unit Test Reports</h3>** 

- **<h3>👋Hands-on Demos</h3>** 

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **🚩GitLab Unit Test Reports**


- **<h3>🛸Integration</h3>**


  - **<h3>📊Report Generation</h3>**


  - **<h3>🖼️Display</h3>** 


- **<h3>✍️Test Frameworks</h3>**


  - **<h3>📝Java, JavaScript, PHP, Go, etc</h3>**


  - **<h3>📚[Unit Test Report Examples](https://docs.gitlab.com/ee/ci/testing/unit_test_report_examples.html)</h3>**




- **<h3>🛢️GitLab CI/CD Pipeline</h3>**




<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌




## **👋Hands-on Demos**

- **<h3>🎯Demo Goals</h3>**

  - **Create/Check GitLab Unit Test Reports in Tests UI and Merge Requests UI**  




- **<h3>🛸Prerequisites</h3>**
  
  - **<h3>🗃️GitLab Account</h3>**
 
  - **<h3>#️⃣Code Branch: [feature-jira-1-24-GitLab-Test-UI](https://gitlab.com/lian.duan.training/dockerspringbootdemo/-/tree/feature-jira-1-24-GitLab-Test-UI?ref_type=heads) </h3>**
  
  - **<h3>🎞️ [How to trigger a GitLab pipeline on Merge?](https://youtu.be/jqC7PNamoGY) </h3>**



<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **👋Hands-on Demos**

- **<h3>👔Create/Check GitLab Unit Test Reports in Tests UI </h3>**

  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/1.jpg'  width="600px"  style="vertical-align:top"></div>

- **<h3>#️⃣Code Example</h3>**
```
  artifacts:
    reports:
      junit: build/test-results/test/*.xml
    expire_in: 1 day
```

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **👋Hands-on Demos**

- **<h3>👔Create/Check GitLab Unit Test Reports in Merge Requests UI  </h3>**


  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/2.jpg'  width="600px"  style="vertical-align:top"></div>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


