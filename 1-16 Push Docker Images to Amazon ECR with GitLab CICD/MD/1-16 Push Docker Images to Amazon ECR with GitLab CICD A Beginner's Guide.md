<br>
<br>      
📌

# **🐳Push Docker Image to Amazon ECR with GitLab**
## **📣Hands-on Demo will Cover:**  

- **<h3>🏵️Set up an AWS ECR</h3>**
 
- **<h3>✍Create an AWS User for ECR</h3>**
  
- **<h3>🗃️Add AWS Authentication Variables in GitLab Project</h3>**
 
- **<h3>👨‍🔧GitLab Job: Build and Push Docker Image to AWS ECR</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>
<br>      
📌


## **🏵️Set up an AWS ECR**

- **<h3>🏪Repository Name</h3>**

  - **🆔springbootdemo**
  
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/ecr.jpg'  width="800px"></div> 
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **✍Create an AWS User for ECR**

- **<h3>🛠️ECRAdmin</h3>**   
  
 - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/permissions.jpg'  width="800px"></div> 
    
 - **<h3>AmazonEC2ContainerRegistryFullAccess 🆚 AmazonEC2ContainerRegistryPowerUser</h3>**  
  
 - **<h3>🗝️Access Key and Secret Access Key</h3>**   
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **🗃️Add AWS Authentication Variables in GitLab Project** 
- **<h3>📊Demo ENV - Share Git Runner: gitlab-runner 15.9.0</h3>** 
- **<h3>🏗️Project: https://gitlab.com/lian.duan.training/dockerspringbootdemo</h3>** 
   
    - **<h3>Branch: feature-1-16-Push-Docker-Image-to-Amazon-ECR-with-GitLab-CI-CD</h3>**
   
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/auth.jpg'  width="800px"></div>  
- 📚[Authenticate GitLab with AWS](https://docs.gitlab.com/ee/ci/cloud_deployment/#authenticate-gitlab-with-aws)

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>
<br>   
<br>
<br>
<br>
<br>
<br>      
📌

## **👨‍🔧GitLab Job: Build and Push Docker Image to AWS ECR**


###  **🔎Job Detail 1️⃣**
  - **Job Image : amazon/aws-cli:2.10.3** 

  - **entrypoint: [""]**  
    - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/1.jpg'  width="800px"></div> 
    - 📚[publish v2 to PyPI](https://github.com/aws/aws-cli/issues/4947) 
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

###  **🔎Job Detail 2️⃣**
  - **amazon-linux-extras install docker**   
    - 📚[Compare package changes between Amazon Linux 2 and Amazon Linux 2023](https://docs.aws.amazon.com/linux/al2023/release-notes/compare-packages.html)   
  - **docker version check**
  - **aws ecr get-login-password**
  - **--password-stdin**
    - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/2.jpg'  width="800px"></div> 

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

###  **🔎Job Detail 3️⃣**
  - **Build and Push Docker Image**
    - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/3.jpg'  width="800px"></div> 


