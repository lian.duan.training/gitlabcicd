# Job & Stage of Pipeline and Hands-on Demo
## Topics 
- Prerequisite   
- What is GitLab CI Pipeline?
- What is Stage?
- What is Job?
- Hands-on Demo
## Prerequisite   
- GitLab Account
  - Add a Credit or Debit Card in the GitLab Account
  - [400 CI/CD minutes per month](https://about.gitlab.com/pricing/) 
  - [Managing your CI/CD Minutes Usage](https://about.gitlab.com/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage)
  - [Usage Quotas](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab)   
- A Project in GitLab  
  - `git clone https://gitlab.com/lian.duan.training/reactuiapp.git`
     - `feature-1-3-Job-and-Stage-of-Pipeline-and-Demo`

## What is GitLab CI Pipeline?
A CI pipeline is a series of steps that streamline the software delivery process.   
  [<img src="./images/pipleline.jpg" width="60%" />]      
- [.gitlab-ci.yml keyword reference](https://docs.gitlab.com/ee/ci/yaml/)   
## What is Stage?
Define when to run the jobs. For example, stages that run tests after stages that compile the code.   
  [<img src="./images/stage.jpg" width="60%" />]    
- Default Stages
    - build
    - test
    - deploy    
  
[<img src="./images/stage-my-demo.jpg" width="60%" />] 
## What is Job?
Define what to do. For example, jobs that compile or test code.    
  [<img src="./images/job.jpg" width="40%" />]   
- [.gitlab-ci.yml keyword reference](https://docs.gitlab.com/ee/ci/yaml/) 

### Hands-on Demo Pipeline    
  [<img src="./images/job-my-demo.jpg" width="60%" />]  
  
 

 
## Hands-on Demo
### .gitlab-ci.yml

```
variables:
    BUILD_DOCKER_IMAGE: "lianduantraining/react-app" 
    BUILD_DOCKER_IMAGE_VERSION: "V5"
    UI_BASE_IMAGE: "node:14.17.4-stretch"
    DOCKER_CLIENT_IMAGE: "docker:20.10.16"
    DOCKER_DIND_IMAGE: "docker:20.10.16-dind"
  # Predefined CI/CD variables, print all pipeline debug info. 
  # CI_DEBUG_TRACE: "true"
stages: 
   - build
   - test
   - docker image process
# Build UI App
ui_build:
  stage: build
  image: "$UI_BASE_IMAGE"   
  # Build React UI APP 
  script:  
     - npm install 
     - npm run build
  # artifacts Src and build result  
  artifacts:
      paths:
          - $CI_PROJECT_DIR  
# Run React Unit Test
ui_unit_test:
  stage: test
  image: "$UI_BASE_IMAGE"   
  script:  
     - npm test 
# Run React Build Test           
ui_build_test:   
  stage: test  
  image: "$UI_BASE_IMAGE"    
  script:  
     - test -f build/index.html        
## Build Docker Image and Push
docker_image_build_push:
  stage: docker image process
 # Variables in the .gitlab-ci.yml file
  image: "$DOCKER_CLIENT_IMAGE"  
  services:
    - "$DOCKER_DIND_IMAGE"
  before_script:
    - echo "GitLab CI/CD | Print all environment variables"
    # Print all variables
    # - env
    # Project CI/CD variables, $CI_REGISTRY_USER, and $CI_REGISTRY_PASSWORD
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" 
  script:  
    # Build Docker Image
    - docker build -t "$BUILD_DOCKER_IMAGE":"$BUILD_DOCKER_IMAGE_VERSION" .
    # Push Docker Image to Dock Hub
    - docker push "$BUILD_DOCKER_IMAGE":"$BUILD_DOCKER_IMAGE_VERSION"
    # ReName tag of Docker Image to latest
    - docker tag "$BUILD_DOCKER_IMAGE":"$BUILD_DOCKER_IMAGE_VERSION" "$BUILD_DOCKER_IMAGE":latest
    # Push Docker Image with the latest tag to Dock Hub
    - docker push "$BUILD_DOCKER_IMAGE":latest

```
### Test Scenarios   
- Happy Path   
   [<img src="./images/happypath.jpg" width="60%" />]   
- Job - Build Test Failure   
   [<img src="./images/unittesterror.jpg" width="60%" />]       
- Job - Build Test Failure with allow_failure: true    
   [<img src="./images/allow_failure.jpg" width="60%" />]   

