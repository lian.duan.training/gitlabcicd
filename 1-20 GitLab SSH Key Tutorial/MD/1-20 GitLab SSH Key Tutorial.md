
<br>

📌

<br> 


# **🦊GitLab SSH Key Tutorial**

# **✍ Generate an SSH Key**
```
git update-git-for-windows
ssh-keygen -t ed25519 -C "<comment>"
exec ssh-agent bash
ssh-add ~/.ssh/id_ed25519
```

**<h3>📚[GitLab Supported SSH key types](https://docs.gitlab.com/ee/user/ssh.html#supported-ssh-key-types)</h3>**

# **🔑Add Public Key to GitLab**
**<h3>#️⃣GitLab => Profile => SSH Keys</h3>**

# **🧬 Git Clone with the SSH Key**
```
git clone git@gitlab.com:lian.duan.training/gitlabcicd.git
```

<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌




# **🩺Troubleshooting on Windows 10 and 11**

**<h3>⛔Could not open a connection to your authentication agent</h3>**

```
cd "/c/Program Files/Git/bin"
exec ssh-agent bash
ssh-add ~/.ssh/id_ed25519
```


<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
<br>
<br>
<br>
<br>
<br> 
<br>
<br>
📌

