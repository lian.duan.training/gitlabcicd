<!--Use GitLab Pipeline to Continuous Deploy Docker Container to AWS ECS Fargate-->
  
## **Topics**    
- <h3>Manual Update Docker Container to AWS ECS Fargate Steps</h3>  
- <h3>How to Use Pipeline to Continuous Deploy Docker Container to AWS ECS Fargate?</h3>  
- <h3>Hands-on Demo Deploy Docker Container to AWS ECS Fargate</h3> 
- <h3>Cleanup AWS Resources</h3>  

## **Manual Update Docker Container to AWS ECS Fargate Steps**   
- <h3>Build Docker Image </h3>  
- <h3>Create a Fargate task definition  </h3> 
- <h3>Update an existing Fargate service  </h3> 
  

## **How to Use Pipeline to Continuous Deploy Docker Container to AWS ECS Fargate?** 
-  Deploy Stage  
     -   Create a new task definition and update an existing service  
         -   AWS CLI      
         -   Gitlab template   

## **Hands-on Demo Deploy Docker Container to AWS ECS Fargate**  
## Overview Target AWS ECS Fargate  
- <image src='./images/aws.jpg'  width="100%">  

## **Hands-on Demo Deploy Docker Container to AWS ECS Fargate**  
### Add Pipeline Variables
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/variables.jpg'  width="100%"></div>  

## **Hands-on Demo Deploy Docker Container to AWS ECS Fargate**   
### CI/CD Pipeline 
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/gitlab.jpg'  width="100%"></div>  

## **Hands-on Demo Deploy Docker Container to AWS ECS Fargate**  
### Deploy Stage 
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/pipeline.jpg'  width="100%"></div> 

## **Hands-on Demo Deploy Docker Container to AWS ECS Fargate**  
### aws ecs describe-task-definition --task-definition "DemoAppTask" --region "us-east-1"  
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);"><image src='./images/json.jpg'  width="50%"></div> 
 

## **Hands-on Demo Deploy Docker Container to AWS ECS Fargate**  
- <h3>Create an Application Load Balancer</h3> 
- <h3>Create a Fargate service</h3> 
- <h3>Verify the Fargate service </h3> 
- <h3>Run Gitlab Pipeline to update Fargate task definition and service</h3> 
- <h3>Verify the update Fargate service </h3>  

## **Cleanup AWS Resources**  
- <h3>Delete ABL</h3> 
- <h3>Delete Service</h3>