# What are the Differences Between Caches and Artifacts in GitLab Pipeline?

## Topics  
- How do I use Caches and Artifacts in React UI Project Pipeline?
- What are the differences between Caches and Artifacts in GitLab Pipeline? 
- How to use Caches and Artifacts perfectly?

## How do I use Caches and Artifacts in React UI Project Pipeline?
 - [<img src="./images/artifacts.jpg" width="70%" />] 

##  What are the differences between Caches and Artifacts in GitLab Pipeline? 

|                 | Caches                                                                                        | Artifacts                                                                            |
|-----------------|-----------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------|
| **Init Status**     |   - Disabled if not defined globally or per job                                               |   - Disabled if not defined per job                                                  |
| **Scope**           |   - Available for all jobs in your .gitlab-ci.yml if enabled globally( S3 or Specific Runner) |   - Can only be enabled per job, not globally                                        |
| **Create/Use**      |   - Can be used in subsequent pipelines by the same job in which the cache was created        |   - Create during a pipeline and can be used by subsequent jobs in the same pipeline |
| **Stored Location** |   - Local cache store in GitLab Runner<br/>  - Distribute cache store in S3                   |   - GitLab                                                                           |
| **Clean Up**        |   - Manually Delete Local cache <br/>       - Manually Delete Distribute cache                              |  - Auto delete after expiration.  <br/>     - A default expiration time is 30 days.  |

##  How to use Caches and Artifacts perfectly?
 - [<img src="./images/use.jpg" width="100%" />] 