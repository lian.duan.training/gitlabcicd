<!--# How to Use Manual Job |Choose when to run jobs-->
📌
# **🎯How to Use Manual Jobs in GitLab?**

## **📣What We Will Cover:**    
- **<h3>🙋🏻‍♂️What is Manual Job in GitLab?</h3>** 
   
- **<h3>📖How to Add a Manual Job in GitLab Pipeline?</h3>**  
  
- **<h3>🌐How to Find Manual Jobs in GitLab UI?</h3>** 

- **<h3>🖐️Hands-on Demo</h3>** 
  
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **🙋🏻‍♂️What is Manual Job in GitLab?**


-  **<h3>👆Task Require Human Intervention</h3>**


   - **<h3>🕹️Click a Button within the GitLab Interface</h3>**


   - **<h3>🤖GitLab API to Trigger</h3>**
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br> 

📌



## **📖How to Add a Manual Job in GitLab Pipeline?**


- **<h3>👨🏻‍💻A Manual Job is in Pipeline</h3>**


  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/code.png'   width="390px"> </div> 
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **🌐How to Find Manual Jobs in GitLab UI?**

  - **<h3>📋Pipelines List</h3>**
  
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/ui1.png'   width="800px"> </div>  

  - **<h3>🔎Pipeline Detail</h3>**
  
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width:fit-content"  width="100%"><image src='./images/ui2.png'   width="400px"> </div> 

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **🖐️Hands-on Demo**

- **<h3>🎯Demo Items</h3>**

  - **<h3>🏗️Trigger a Build in feature-1-12-How-to-Use-Manual-Jobs-in-GitLab</h3>**
    
  - **<h3>🛠️Trigger a Build in Main Branch</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌