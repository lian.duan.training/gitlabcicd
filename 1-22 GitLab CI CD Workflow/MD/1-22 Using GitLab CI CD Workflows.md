
<br>
<br>    
📌


# **🦊Using GitLab CI/CD Workflow**

## **🕵What is GitLab CI/CD Workflow?**

**<h3>🧮GitLab CI/CD workflow allows us to control the conditions under which pipelines are created and executed.</h3>** 


- **<h3>📑Controlling/Conditions Pipeline Creation</h3>**
 
- **<h3>🤸‍♂️Flexibility</h3>**
 
- **<h3>✂️Simplifying Pipeline Configuration</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **🔀Workflow Syntax**

```
workflow:
  rules:
    - if: <condition>
      when: <when>
        on_success
        on_failure
        always
        manual
        allow_failure
      variables:<variables>
    # Additional rules...
```

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **🛢️Demo Pipeline** 
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/old-branch.jpg'  width="1000px"  style="vertical-align:top"></div>
  
- **<h3>#️⃣[.gitlab-ci-original.yml]()</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **📝Workflow Rules: Evaluation Sequence and The First Matching Rule**


- **<h3>⛔Incorrect</h3>**


- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/error.jpg'  width="800px"  style="vertical-align:top"></div>
  

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **📝Workflow Rules: Evaluation Sequence and The First Matching Rule**

- **<h3>💯Correct</h3>**
 
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/sq.jpg'  width="600px"  style="vertical-align:top"></div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **🪜Restructured Demo Pipeline Use Workflow**
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/change.jpg'  width="900px"  style="vertical-align:top"></div>
  
- **<h3>#️⃣[.gitlab-ci-workflow.yml](.gitlab-ci-workflow.yml)</h3>**


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **📊Test Workflow Rules**

- **<h3>💡feature branch</h3>**

- **<h3>🎉release branch</h3>**

- **<h3>💪main branch</h3>**


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

