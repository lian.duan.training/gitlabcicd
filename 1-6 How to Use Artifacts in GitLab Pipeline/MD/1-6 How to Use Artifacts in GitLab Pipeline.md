# How to Use Artifacts in GitLab Pipeline? 
## Topics 
- Prerequisite 
- What is Artifacts in Pipeline? 
- How to Add Artifacts in Pipeline?
- How to Check Artifacts in Pipeline? 
- How to Delete Job Artifacts in GitLab?  
## Prerequisite  
- GitLab Account
  - Add a Credit or Debit Card to the GitLab Account
  - [400 CI/CD minutes per month](https://about.gitlab.com/pricing/) 
  - 5 GB Limit for Free Namespaces 
  - [Usage Quotas](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab)   
- A React UI Project in GitLab  
  - `git clone https://gitlab.com/lian.duan.training/reactuiapp.git`
     - `feature-1-6-How-to-Use-Artifacts-in-GitLab-Pipeline`  

## What is Artifacts in Pipeline? 
Pipeline artifacts are files created by GitLab after a pipeline finishes. 
 - [<img src="./images/artifacts.jpg" width="70%" />] 
## How to Add Artifacts in Pipeline?
- Add Artifact in Job
 - [<img src="./images/code.jpg" width="50%" />]   
- Run a Pipeline   
## How to Check Artifacts in Pipeline?
 - [<img src="./images/download.jpg" width="80%" />] 
 
## How to Delete Job Artifacts in GitLab?   
- [<img src="./images/delete.jpg" width="20%" />]
- [<img src="./images/How to Quickly Delete Job Artifacts in GitLab.png" width="60%" />]