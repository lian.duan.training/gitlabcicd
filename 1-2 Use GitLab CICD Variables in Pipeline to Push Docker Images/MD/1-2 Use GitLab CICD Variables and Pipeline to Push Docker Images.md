# Use GitLab CI/CD Variables in Pipeline to Push Docker Images

## Topics  
- Prerequisite
- What is GitLab Variable?
- GitLab CI/CD Basic Variable Types 
- GitLab CI/CD Variable Overwrite  
- How to Use Variable in Pipeline?
- How to Print All Variables in Pipeline?  
- Create a GitLab Pipeline with Variables to Build and Push Docker Images
- Check Pipeline Running Result

## Prerequisite   
- GitLab Account
  - Add Credit or Debit Card in the GitLab Account
  - [400 CI/CD minutes per month](https://about.gitlab.com/pricing/) 
  - [Managing your CI/CD Minutes Usage](https://about.gitlab.com/pricing/faq-consumption-cicd/#managing-your-cicd-minutes-usage)
  - [Usage Quotas](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab)   
- A Project in GitLab  
  - `git clone https://gitlab.com/lian.duan.training/reactuiapp.git`
     - `feature-1-2-Use-GitLab-CI/CD-Variables-and-Pipeline-to-Push-Docker-Images` 

## What is GitLab CI/CD Variable?
GitLab variables provide developers with the ability to configure values in their code.    

## GitLab CI/CD Basic Variable Types 
- [Predefined CI/CD variables](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)   
    - `CI_DEBUG_TRACE: "true"`  
       - GitLab Version: all    
       - Runner Version: 1.7       
- Custom Define Variables   
  - Variables in the .gitlab-ci.yml file
   ``` 
  variables:
    BUILD_DOCKER_IMAGE: "lianduantraining/react-app" 
    BUILD_DOCKER_IMAGE_VERSION: "V4"
   ```  
  - Project CI/CD variables  
    - Project-> Setting-> CI/CD
    - [<img src="./images/projectCICDvariables.jpg" width="60%" />]
    - 
      ```
        # Project CI/CD variables, $CI_REGISTRY_USER and $CI_REGISTRY_PASSWORD   
         docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD"         
  - Group CI/CD variables
  - Instance CI/CD variables

## GitLab CI/CD Variable Overwrite
[<img src="./images/overwrite.jpg" width="60%" />]
## How to Use Variable in Pipeline?
   - "$Variable Name" 
## How to Print All Variables in Pipeline?
  ```
    - echo "GitLab CI/CD | Print all environment variables"
    - env
  ```
## Create a GitLab Pipeline with Variables to Build and Push Docker Images
[<img src="./images/yaml.jpg" width="90%" />]
## Check Pipeline Running Result
- GitLab   
- Docker Hub   

