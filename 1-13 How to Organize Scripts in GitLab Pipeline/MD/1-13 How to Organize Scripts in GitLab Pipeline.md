📌
# **🎯How to Organize Scripts in GitLab Pipeline?** 
## **🤔Hands-on Demo Steps**  
   
- **<h3>🧬Git Clone: https://gitlab.com/LianDuanTrainingGroup/common-lib.git</h3>**

  - **<h3>🔀 Branch: Main</h3>**  
  
  
- **<h3>👨🏻‍💻 Analysis Linux Commands in Pipeline </h3>**
  
- **<h3>🗂️ Create Script Folders in Project</h3>**
     
- **<h3>📦 Move Linux Commands from Pipeline to Script File</h3>**
    
- **<h3>🐛 Error Catch in Script File</h3>**
  
- **<h3>🐞 Add Debug Info in Script File</h3>**
  
- **<h3>🚦  Add Execution Permission to the Script File </h3>** 
  
- **<h3>🧪 Test Pipeline  </h3>**
   
