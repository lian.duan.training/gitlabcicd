<br>
<br>    
📌


# **🦊How to Reuse GitLab Jobs?**

## **What We Will Cover**：

- **<h3>🤔What is GitLab Shared Library?</h3>**
  
- **<h3>👉Hands-on Demo</h3>**
  
  - **🔎Demo Project Quick View**  
  - **📚Create a GitLab Shared Library**  
  - **🕵️‍♀️Find Reusable Jobs**  
  - **🎯Promote Reusable Jobs** 
  - **💥Enhance Demo Project to Use GitLab Shared Library**  


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **🤔What is GitLab Shared Library?**
**<h3>GitLab Shared Library is a feature in GitLab that allows you to define and share reusable code across multiple GitLab projects.</h3>**

**<h3>Three Ways to Use GitLab Shared Library.</h3>**


- **Git Submodule**
  


  
```
git submodule add /path/to/my-submodule
```        



- **Git Clone**


  
  - shared-lib-install-tools.yml


```
shared-lib-install-tools:
  stage: setup
  script:
    - git clone https://gitlab.com/my-demo/shared-lib.git
```



- **Include Project**
  


```   
  - project: 'LianDuanTrainingGroup/demosharedlibrary'
    ref: main
    file: 
      - 'templates/XXX.yml'
      - 'vars/YYY.yml'

```

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **🔎Demo Project Quick View**  

- **<h3>Demo ENV: </h3>**
  -  **Share Git Runner: gitlab-runner 15.9.0**
  
- **<h3>Project: </h3>**
    - **https://gitlab.com/lian.duan.training/dockerspringbootdemo**
  
    - **Branch: feature-1-17-How-to-Reuse-GitLab-Jobs**
- 🎦[Use GitLab CI/CD Variables and Pipeline to Push Docker Images](https://youtu.be/lwDYRSdkTrk)
- 🎦[Push Docker Image to Amazon ECR with GitLab](https://youtu.be/lwDYRSdkTrk)

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **📚Create a GitLab Shared Library**

- **<h3>Project:</h3>** 


    - **https://gitlab.com/LianDuanTrainingGroup/demosharedlibrary**
    - **Branch: main**

```  
  shared-library/   
    ├── README.md   
    ├── vars/   
    │   ├── global.yml   
    │   ├── dev.yml   
    │   └── prod.yml   
    └── templates/   
        ├── build.yml   
        ├── test.yml   
        └── deploy.yml   
``` 


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌




## **🕵️‍♀️Find Reusable Jobs**

<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/pipeline.png'  width="800px"></div>  



<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **🎯Promote Reusable Jobs**
### **Job: build_backend_API_image** 
- 🎦[Efficiently Sharing Docker Images between GitLab CI/CD Jobs](https://youtu.be/FLed6nXrqfM)

<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/oldjob.png'  width="800px"></div>


  

### **Extract Variables**

**Global Value => global.yml**  

<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/newjobgvar.png'  width="400px"></div>

**Other Value = > dev.yml and prod.yml** 

<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/newjobvar.png'  width="400px"></div>

### **Use New Values in templates/build.yml**
<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/newjob.png'  width="800px"></div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

##  **💥Enhance Demo Project to Use GitLab Shared Library**

- **Variables OverWrite**

<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/value.png'  width="600px"></div>

- **Include YML Files**

<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/include.png'  width="600px"></div>

- **Job build_backend_API_image Detail**

<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/job.png'  width="300px"></div>

- **Triage Project CI/CD Pipeline to Test** 


