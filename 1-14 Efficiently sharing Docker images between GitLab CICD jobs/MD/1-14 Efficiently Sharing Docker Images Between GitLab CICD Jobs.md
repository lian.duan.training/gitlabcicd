# **💥Efficiently Sharing Docker Image Between GitLab CI/CD Jobs** 

## **🤔Why do I need Sharing Docker Image Between GitLab CI/CD Jobs?**  
<div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25);  width="100%""><image src='./images/pipeline.jpg'   width="1000px"></div>

- **<h3>🏗️Project: https://gitlab.com/lian.duan.training/dockerspringbootdemo</h3>**  
    - **<h3>Branch:feature-1-14-Efficiently-Sharing-Docker-Image-Between-GitLab-CI-CD-Jobs</h3>**
      


- **<h3>🚀Faster build times</h3>**
- **<h3>🔑Consistent environment</h3>**
- **<h3>✂️Simplified maintenance</h3>**
  
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **📖How to Share Docker Image Between GitLab CI/CD Jobs?**
-  **<h3>🐳Docker Registry</h3>**
   -  Job A: 
         - Build and Push a Docker Image 
   -  Job B: 
         - Pull and Use the Docker Image  
   -  Job C: 
         - Pull and Use the Docker Image  

-  **<h3>🏺Artifacts</h3>**
   -  Job A: 
         - Build a Docker Image
         - Save the Docker Image to GitLab artifacts 
   -  Job B: 
         - Load the Docker Image form artifacts and Use the Docker Image  
   -  Job C: 
         - Load the Docker Image form artifacts and Use the Docker Image 

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **🖐️Hands-on Demo**
- **<h3>🛠️build_backend_API_image Job</h3>**
  - `docker save -o <output-file-name.tar> <image-name>` 
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/save.jpg'  width="800px"></div> 
  - 🎥[Build Docker Images in GitLab CI Pipeline](https://youtu.be/noGcDULs9EA) 
  - 🎥[How to Use Artifacts in GitLab Pipeline?](https://youtu.be/E6FLu-JmMS4)   



- **<h3>📝Test_backend_API_image Job</h3>**
  - `docker load -q -i <input-file-name.tar>`  
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content;"  width="100%"><image src='./images/load.jpg'  width="1000px"></div> 
  - 🎥[How (and Why) to Add a Docker Container Health Check?](https://youtu.be/1nxxlkxqktU)


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌