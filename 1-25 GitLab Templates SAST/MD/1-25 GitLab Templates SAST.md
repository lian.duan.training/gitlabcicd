<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

 
# **🦊 GitLab Build-in Templates**

## **📤Topics:**

- **<h3>🪂What are GitLab CI/CD Build-in Templates?</h3>**


- **<h3>📤How to Use Build-in Templates?</h3>**


- **<h3>📝How to Overwrite Build-in Templates?</h3>**


- **<h3>💪Static Application Security Testing in Action</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌




# **🪂What are GitLab CI/CD Build-in Templates?**


- **<h3>🛃Predefined Configurations</h3>**


- **<h3>🗒️Specialized Templates</h3>**


- **<h3>⏺[Templates](https://gitlab.com/gitlab-org/gitlab/-/tree/v16.5.0-rc43-ee/lib/gitlab/ci/templates)</h3>**


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



# **📤How to Use Build-in Templates?**

-  **<h3>🛠️Setting up the .gitlab-ci.yml File </h3>** 


    ```
    include:
      - template: Jobs/SAST.gitlab-ci.yml
    ```
-  **<h3>🗂️[SAST.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/v16.5.0-rc43-ee/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml?ref_type=tags)</h3>**


    - **<h3>👨🏽‍🏫18 Jobs</h3>**


    -  **<h3>🔖Tag is v16.5.0-rc43-ee</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



# **📝How to Overwrite Build-in Templates?**

- **<h3>🔎Create a job with the same name in Build in Templates</h3>** 


- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/1.jpg'  width="800px"  style="vertical-align:top"></div>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


# **💪SAST in Action**

- **<h3>🐞Scanning for Vulnerabilities</h3>**


- **<h3>📊Reporting the Findings</h3>**


- **<h3>🔒Keeping Our Software Safe</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



# **💪SAST in Action**
- **<h3>🛸Prerequisites </h3>**


  - **<h3>🗃️Free GitLab Account</h3>**


  - **<h3>🌀Demo Project GitLab Version -  Enterprise Edition 16.6.0-pre e4d8e46684b</h3>** 
 
  - **<h3>#️⃣[feature-1-25-GitLab-Build-in-Templates](https://gitlab.com/lian.duan.training/dockerspringbootdemo/-/commits/feature-1-25-GitLab-Build-in-Templates)</h3>** 

  - **<h3>⏺[SAST.gitlab-ci.yml](https://gitlab.com/gitlab-org/gitlab/-/blob/v16.5.0-rc43-ee/lib/gitlab/ci/templates/Jobs/SAST.gitlab-ci.yml?ref_type=tags)</h3>**


  - **<h3>🏄‍♂️[Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌




# **💪SAST in Action**


- **<h3>🥇Demo Goals</h3>**


  - **<h3>🪲Enable SAST Debug</h3>**
  
    ```
    SECURE_LOG_LEVEL: "debug"
    ```


  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/3.jpg'  width="600px"  style="vertical-align:top"></div>

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


  - **<h3>🐳Overwrite SAST Docker Image and Verify Scan Result</h3>**


    ```
    semgrep-sast:
      variables:
        SAST_ANALYZER_IMAGE_TAG: "4.5.0"
    ```

  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/2.jpg'  width="600px"  style="vertical-align:top"></div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

