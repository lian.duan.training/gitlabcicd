# How to Keep GitLab Branches Safer in Project? | GitLab Protect Branch | Protect Variable
## **Topics**
- <h3>What is Gitlab Protected Branch?</h3> 
- <h3>How to Keep GitLab Branches Safer in your project? </h3> 
- <h3>Hands-on Demo</h3> 


## **What is Gitlab Protected Branch?** 
- Prevents pushes from everybody except users with Maintainer permission  
- Prevents anyone from force pushing to the branch  
- Prevents anyone from deleting the branch
  
## **How to Keep GitLab Branches Safer in your project?**   
- Branching Strategies   
    - GitFlow   
    - GitHub Flow   
    - GitLab Flow   
    - Trunk-based development   
- Protect Branch   
- Protect Variable   


## **Hands-on Demo**
- Add Protect Branch with GitHub Flow
- Add/Change Protect Variable
