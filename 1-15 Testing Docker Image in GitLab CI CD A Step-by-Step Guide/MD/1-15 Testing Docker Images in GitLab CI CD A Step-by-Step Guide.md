# **🐳Testing Docker Image in GitLab CI/CD | A Step-by-Step Guide**

## **📊Demo ENV & Project**
- **<h3>🔗Share Git Runner: gitlab-runner 15.9.0</h3>**
- **<h3>🏗️Project: https://gitlab.com/lian.duan.training/dockerspringbootdemo</h3>**  
    - **<h3>🪵Branch: feature-1-15-Testing-Docker-Image-in-GitLab-CI-CD</h3>**
- **<h3>🚧Pipeline</h3>**
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/pipeline.jpg'  width="800px"></div> 
    
  - 🎥[Build Docker Images in GitLab CI Pipeline](https://youtu.be/noGcDULs9EA)   
  
  - 🎥[How to Use Artifacts in GitLab Pipeline?](https://youtu.be/E6FLu-JmMS4)    

  - 🎥[Efficiently Sharing Docker Image Between GitLab CI/CD Jobs](https://youtu.be/E6FLu-JmMS4)
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


##  **🚀Running Quick Testing Before Pushing Docker Image to Repository**
- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/1.jpg'  width="800px"></div>
- 🎥[How (and Why) to Add a Docker Container Health Check?](https://youtu.be/1nxxlkxqktU) 
  <br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **💻Automated Testing of Docker Image with Postman/Newman**

- <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/2.jpg'  width="800px"></div>
- 📙[Useless docker image for the GitLab CI/CD Pipelines. #1948](https://github.com/postmanlabs/newman/issues/1948)
- 📙[Override the entrypoint of an image](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#where-scripts-are-executed)

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


