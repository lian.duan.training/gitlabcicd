# Create a Free GitLab Runner in Oracle Cloud
## Topics 
- Pre-request   
- What is GitLab Runner?
- What is the Executor of GitLab Runner?
- Install and Register GitLab Runner in Instance of Oracle Cloud 
- Use the GitLab Runner to Build a React UI Project

## Pre-request
- GitLab Account
  - Add a Credit or Debit Card to the GitLab Account
  - [400 CI/CD minutes per month](https://about.gitlab.com/pricing/) 
  - [Usage Quotas](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab)   
- A React UI Project in GitLab  
  - `git clone https://gitlab.com/lian.duan.training/reactuiapp.git`
     - `feature-1-4-Create-a-Free-GitLab-Runner-in-Oracle-Cloud`   
- A Ubuntu Instance in Oracle Cloud
   - [How to Create a Free Instances in Oracle Cloud](https://youtu.be/s8k0ZeS_qGU)
   - [<img src="./images/How to Create a Free Instances in Oracle Cloud.png" width="70%" />] 
  
## What is GitLab Runner?
GitLab Runner is an application that works with GitLab CI/CD to run jobs in a pipeline.

### Runner Types
- Shared runner 
- Group runner  
- Specific runner 
  - Add tags when a runner is  registered   

## What is the Executor of GitLab Runner?    
GitLab Runner implements several executors that can be used to run builds in different scenarios.       
[<img src="./images/c-chart.jpg" width="70%" />]    


## Install and Register GitLab Runner in Instance of Oracle Cloud
### Install a GitLab Runner
```   
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb
gitlab-runner --version
gitlab-runner list
``` 
### Register a GitLab Runner

```
sudo gitlab-runner register
```    
[<img src="./images/reg.jpg" width="100%" />] 
- Check Runner Status 
  - GitLab => Project => Setting => Runners => Specific runners   
  - [<img src="./images/gitlabstatus.jpg" width="50%" />]        

## Use the GitLab Runner to Build a React UI  Project
- Add a Tag in the Job
  - [<img src="./images/codeTag.jpg" width="20%" />]
- Trigger a Build Pipeline