# Use Cache to Speed Up Job Execution in GitLab  
## Topics 
- Prerequisite   
- What is GitLab Cache in Pipeline?  
- GitLab Cache Policies and Types
- GitLab Cache Scopes
- How to Use GitLab Cache in Pipeline?
- Where is GitLab Cache Stored?
- How to Clean Up GitLab Runner Cache?  
- Hands-on Demo
## Prerequisite   
- GitLab Account
  - Add a Credit or Debit Card to the GitLab Account
  - [400 CI/CD minutes per month](https://about.gitlab.com/pricing/) 
  - [Usage Quotas](https://gitlab.com/-/profile/usage_quotas#pipelines-quota-tab)   
- A React UI Project in GitLab  
  - `git clone https://gitlab.com/lian.duan.training/reactuiapp.git`
     - `feature-1-5-Use-Cache--to-Speed-Up-Job-Execution-in-GitLab`  
- A Specific GitLab Runner in Oracle Cloud
   - [Create a Free GitLab Runner in Oracle Cloud](https://youtu.be/s8k0ZeS_qGU)
   - [<img src="./images/Create A Free Gitlab Runner In Oracle Cloud.png" width="40%" />] 


## What is GitLab Cache in Pipeline?  
A cache is one or more files a job downloads and saves. Subsequent jobs that use the same cache don’t have to download the files again, so they execute more quickly.   

 - [<img src="./images/cacheinlog.jpg" width="90%" />]
 - [<img src="./images/pipeline.jpg" width="70%" />] 


## GitLab Cache Policies and Types
- Cache Policies
  - Push-pull 
  - Pull  
  - Push 
- Cache Types
  - Distributed cache
  - Local cache
  - [<img src="./images/cacheinlog.jpg" width="90%" />]
## GitLab Cache Scopes
- Project  
- Branch  
- Build  

## How to Use GitLab Cache in Pipeline?
- Add Cache Configuration in Jobs 
  - Job: install_ui_build_cache    
  - Job: ui build  

 - [<img src="./images/pipeline.jpg" width="70%" />] 

- Create/update the cache in Job - install_ui_build_cache
```
# Install UI Build Cache
install_ui_build_cache:
  stage: init 
  # init npm cache 
  script:  
    - npm install 
  cache:
    untracked: true  
    key: binaries-cache-${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
    policy: pull-push     
  tags:
    - oracle_cloud_free_runner  
```

- Use the cache in Job - ui_build
```
# Build UI App
ui_build:
  stage: build  
  # Build React UI APP 
  script:  
    - npm run build
  cache:
    untracked: true   
    key: binaries-cache-${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/ 
    policy: pull   
  # artifacts Src and build result  
  artifacts:
      paths:
          - $CI_PROJECT_DIR 
  tags:
    - oracle_cloud_free_runner  
```

## Where is GitLab Cache Stored?
Local cache is stored under the gitlab-runner user's home directory: `/home/gitlab-runner/cache/<user>/<project>/<cache-key>/cache. zip` .       

`sudo ls /home/gitlab-runner/cache/lian.duan.training/reactuiapp/`  
 
 [<img src="./images/cachePath.jpg" width="80%" />]   
 [<img src="./images/cachePath-1.jpg" width="80%" />]    


## How to Clean Up GitLab Runner Cache? 

[<img src="./images/clearup.jpg" width="60%" />]  
[<img src="./images/clearup-1.jpg" width="90%" />]

## Hands-on Demo
- Run Job two times  
  - First Job Run Time 
     - [<img src="./images/firstrun.jpg" width="60%" />]
  - Second Job Run Time    
     - [<img src="./images/secondrun.jpg" width="60%" />]   