
<br>
<br>    
📌


# **🦊Speed Up Pipeline with Needs Relationships**

## **👨‍🏫Demo Goal:**
  - **<h3>Add Needs Relationships to Speed Up Pipeline</h3>**
   
    - **<h3>🚀Reduce Build Time From 9 Mins to 5 Mins</h3>**
     
    - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/goal.jpg'  width="550px"></div>
    - **<h3>🪂Needs</h3>**
    - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/goal2.jpg'  width="550px"></div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌




## **🤔What is Needs in GitLb Pipeline?** 
**<h3>📝The "needs" keyword in GitLab CI/CD is used to define dependencies between different jobs within a pipeline. </h3>**

```
stages:
  - build
  - test
  - deploy

build_job:
  stage: build
  script:
    - echo "Building..."
  # No needs specified, so it runs on its own

test_job:
  stage: test
  script:
    - echo "Testing..."
  needs:
    - build_job
...
```
**<h3>📝Needs Relationships Can Speed Up GitLab Pipeline</h3>**


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **🛤️Analysis Demo Pipeline**

  - 📟[.gitlab-ci-original.yml](https://gitlab.com/lian.duan.training/gitlabcicd/-/blob/main/1-21%20Speed%20Up%20Pipelines%20with%20Needs/pipeline/.gitlab-ci-original.yml?ref_type=heads)
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/pipeline.jpg'  width="800px"></div>  




    | Job Name | Running Time | Dependent Job |
    | :-----| :----- | :----- |
    | common_jar_x | 10 s | N |
    | common_jar_y | 120 s |N |
    | common_jar_z1 | 10 s |  common_jar_x |
    | common_jar_z2 | 120 s | common_jar_y |
    | build_ui | 30 s | N |
    | build_backend_api | 60 s | common_jar_z1 |
    | test_ui | 30 s | build_ui |
    | test_backend_api | 60 s | build_backend_api |
    | deploy_ui_to_stage_ENV | 30 s | test_ui  |
    | deploy_backend_api_stage_ENV | 60 s | test_backend_api |

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **💼Add Needs Into Jobs in Demo Pipeline**

  - 📟[.gitlab-ci-add-needs.yml](https://gitlab.com/lian.duan.training/gitlabcicd/-/blob/main/1-21%20Speed%20Up%20Pipelines%20with%20Needs/pipeline/..gitlab-ci-add-needs.yml?ref_type=heads) 
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/pipeline_new_02.jpg'  width="800px"></div>  

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

