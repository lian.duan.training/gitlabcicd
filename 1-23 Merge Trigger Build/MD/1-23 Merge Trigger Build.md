
<br>
<br>    
📌
<!--How to trigger a Gitlab pipeline on Merge|Trigger Pipeline Using RULES | GitLab Tutorial-->

# **🦊How to trigger a GitLab pipeline on Merge?**
## **📤Topics:**


- **<h3>🚩Use GitLab Builtin Value Trigger Pipeline</h3>**

- **<h3>🎖️Benefit Merged Trigger Build</h3>**

- **<h3>👋Hands-on Demos</h3>**

<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌



## **🚩Use GitLab Builtin Value Trigger Pipeline** 

- **<h3>🔢$CI_PIPELINE_SOURCE</h3>**
  
- **<h3>🎊Events</h3>**
  
  - **🎄Merge Request**
    - $CI_PIPELINE_SOURCE == "merge_request_event" 
  - **🎠Push to Repository Branch**
    - $CI_PIPELINE_SOURCE == "push" 
  - **⏲Scheduled**
    - $CI_PIPELINE_SOURCE == "schedule"   
  - **✍️Manual Run**
    - $CI_PIPELINE_SOURCE == "web" || $CI_PIPELINE_SOURCE == "api"   



<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **🎖️Benefit Merged Trigger Build**  

- **<h3>🌄Early Testing and Validation</h3>**


- **<h3>🈸Isolation of Changes</h3>**


- **<h3>🦺Code Quality Assurance</h3>**


<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **👋Hands-on Demos**

- **<h3>🎯Demo Goal</h3>** 
**📋Trigger Pipeline's Build Job and Test Job When Code Merge from feature-XXX to release-XXX**





 
- **<h3>🛸Prerequisites</h3>**
  
  - **<h3>🗃️GitLab Account</h3>**
 
  - **<h3>📊A Project in GitLab </h3>**
  
  - **<h3>🎞️Pipeline Files are based on [Using GitLab CI/CD Workflow](https://youtu.be/jqC7PNamoGY) </h3>**


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>
<br>   
<br>
<br>
<br>      
📌


## **👋Hands-on Demos**


- **<h3>👔Add Merge Rule into Jobs</h3>**


  - 📗[.gitlab-ci-original.yml
](https://gitlab.com/lian.duan.training/gitlabcicd/-/blob/main/1-22%20GitLab%20CI%20CD%20Workflow/Pipeline/.gitlab-ci-original.yml?ref_type=heads)
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/1.jpg'  width="600px"  style="vertical-align:top"></div>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>
<br>      
📌

## **👋Hands-on Demos**

- **<h3>📒Add Merge Rule into Workflow</h3>**



  - 📗[.gitlab-ci-workflow.yml
](https://gitlab.com/lian.duan.training/gitlabcicd/-/blob/main/1-22%20GitLab%20CI%20CD%20Workflow/Pipeline/.gitlab-ci-workflow.yml?ref_type=heads)
  - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); width:fit-content"  width="100%"><image src='./images/2.jpg'  width="600px"  style="vertical-align:top"></div>


<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>   
<br>
<br>
<br>   
<br>
<br>   
<br>
<br>
<br>      
📌