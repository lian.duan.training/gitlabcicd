# How to Publish a Java Project to GitLab Package Registry? | Use Gradle Build And Publish Package to Registry in GitLab

## **Topics**
- <h3>What is Package Registry in GitLab?</h3> 
- <h3> How to Use Gradle Build and Publish a Jar to Registry Package to GitLab?</h3> 
- <h3>Hands-on Demo</h3> 

## **What is Package Registry in GitLab?**    

-  <h4>Private or public registry </h4>   
-  <h4>Supported package management </h4>   
-  <h4>Publish and share packages</h4>   
-  <h4>Project => Packages and registries => Package Registry </h4>   
     - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/file6.jpg'  width="80%"></div>    
 

## **How to Use Gradle Build and Publish a Jar to Registry Package to GitLab?**

- <h3>Create a Deploy Token </h3>
- <h3>Create a CI/CD variable to store the Deploy Token </h3>
- <h3>Add Publishing in build.gradle</h3>
   - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/file1.jpg'  width="100%"></div>
- <h3>Add build detail into the Manifest</h3>
   - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/file2.jpg'  width="100%"></div>

- <h3>Run gradlew publish </h3>

## **Hands-on Demo**
- Common-lib Project Structure
   - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/file3.jpg'  hight="40%" width="40%"></div> 

- Build Stage in .gitlab-ci.yml
   - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/file4.jpg'  width="40%"></div> 

- Deploy Stage in .gitlab-ci.yml 
   - <div id="wrapper-div" style="box-shadow: 0px 2px 25px rgba(0, 0, 0, .25); "><image src='./images/file5.jpg'  width="80%"></div> 